var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    svgSprite = require("gulp-svg-sprite"),
    spritesmith = require("gulp.spritesmith"),
    htmlmin = require('gulp-htmlmin'),
    minifycss = require('gulp-clean-css'),
    cssnano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    notify = require('gulp-notify'),
    filter = require('gulp-filter'),
    order = require('gulp-order'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    watch = require('gulp-watch');


// SVG Config
var config = {
  shape: {
    dimension: {// Set maximum dimensions
        maxWidth: 32
      },
  },
  mode: {
    symbol: { // symbol mode to build the SVG
      render: {
        css: false, // CSS output option for icon sizing
        scss: true // SCSS output option for icon sizing
      },
      dest: 'sprite', // destination folder
      prefix: '.svg--%s', // BEM-style prefix if styles rendered
      sprite: 'sprite.svg', //generated sprite name
      example: true // Build a sample page, please!
    }
  }
};


// SCSS Compile
gulp.task('sass', function() {
    return gulp.src('app/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(cssnano({
          discardComments: {
            removeAll: true
          },
          discardDuplicates: true,
          discardUnused: true,
          autoprefixer: true,
          core: true,
          mergeLonghand: true,
          mergeRules: true,
          minifySelectors: true,
          filterPlugins: true
        }))
        .pipe(sourcemaps.write('sourcemaps'))
        .pipe(gulp.dest('dist/css'))
        .pipe(notify({ message: 'Scss task complete' }));
});

// SVG Sprite
gulp.task('svg-sprite', function() {
    gulp.src('app/icons/*.svg')
        .pipe(svgSprite(config))
        .pipe(gulp.dest("app"));
});
gulp.task('sprite-scss', function() {
    gulp.src('app/sprite/sprite.scss')
        .pipe(rename('_svg-sprite.scss'))
        .pipe(gulp.dest('app/scss/modules'));
});
gulp.task('sprite-shortcut', ['svg-sprite', 'sprite-scss'], function() {
    gulp.src('app/sprite/sprite.svg')
        .pipe(gulp.dest('dist/images/icons'));
});

// HTML Minifier
gulp.task('html', function() {
    gulp.src('app/pages/*.php')
        .pipe(htmlmin({
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          decodeEntities: true,
          keepClosingSlash: true,
          minifyCSS: true,
          minifyJS: true,
          processConditionalComments: true,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          sortAttributes: true,
          sortClassName: true,
          useShortDoctype: true
        }))
        .pipe(gulp.dest('dist'))
});
gulp.task('html-includes', function() {
    gulp.src('app/pages/includes/*.php')
        .pipe(htmlmin({
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          decodeEntities: true,
          keepClosingSlash: true,
          minifyCSS: true,
          minifyJS: true,
          processConditionalComments: true,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          sortAttributes: true,
          sortClassName: true,
          useShortDoctype: true
        }))
        .pipe(gulp.dest('dist/includes'))
});

// JS Concat
gulp.task('js', function() {

	var jsFiles = ['app/js/*'];

	gulp.src(jsFiles)
		.pipe(filter('*.js'))
    .pipe(order([
      'jquery.min.js',
      'wow.min.js',
			'*',
      'scripts.js'
		]))
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist/js'))
    .pipe(notify({ message: 'JS task complete' }));
});

// Move Twitter folder to dist folder
gulp.task('move-twitter', function() {
    gulp.src('app/twitter/**/', { base: 'app/' })
        .pipe(gulp.dest('dist'));
});

// Move Forms folder to dist folder
gulp.task('move-forms', function() {
    gulp.src('app/forms/**/', { base: 'app/' })
        .pipe(gulp.dest('dist'));
});

// Watch
gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('app/scss/**/*', ['sass']);

  // Watch .php files
  gulp.watch('app/pages/**/*.php', ['html', 'html-includes']);

  // Watch .js files
  gulp.watch('app/js/*', ['js']);

  // Watch Twitter folder
  gulp.watch('app/twitter/**/', ['move-twitter']);

  // Watch forms folder
  gulp.watch('app/forms/**/', ['move-forms']);

});

// Default task
gulp.task('default', function() {
    gulp.start('css', 'js', 'watch');
});
