<!--******************** SITE FOOTER : Start ********************-->
<footer class="site-footer">
  <!-- BEGIN : CONTAINER -->
  <div class="container">
    <!-- BEGIN : ROW -->
    <div class="row">
      <!-- BEGIN : COL -->
      <div class="col">
        <!-- BEGIN : FLEX CONTAINER -->
        <div class="flex-container">
          <!-- BEGIN : FOOTER item -->
          <div class="footer-item">
            <h4>Quick links</h4>
            <ul class="quick-links">
              <li><a href="index.php">Physio Baby</a></li>
              <li><a href="#">Email Login</a></li>
              <li><a href="resources.php">Resources</a></li>
              <li><a href="site-map.php">Sitemap</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </div>
          <!-- END : FOOTER ITEM -->
          <!-- BEGIN : FOOTER ITEM -->
          <div class="footer-item">
            <h4>Contact</h4>
            <ul class="quick-links">
              <li>
                <ul>
                  <li>Louise Jewers Physiotherapy</li>
                  <li>Hogarth Health Club</li>
                  <li>Airedale Avenue</li>
                  <li>Chiswick</li>
                  <li>W4 2NW</li>
                </ul>
              </li>
              <li>
                <svg class="svg--call-dims">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#call"></use>
                </svg>
                <b>020 8742 7423</b></li>
              <li>
                <svg class="svg--mail-dims">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#mail"></use>
                </svg>
                <a href="mailto:info@louisejewersphysio.co.uk">info@louisejewersphysio.co.uk</a></li>
            </ul>
          </div>
          <!-- END : FOOTER ITEM -->
          <!-- BEGIN : FOOTER ITEM -->
          <div class="footer-item">
            <h4>Follow us</h4>
            <div class="footer-social">
              <!-- <a href="#" class="social-item icon-facebook" title="Facebook"><i class="fa fa-facebook"></i></a> -->
              <a href="https://twitter.com/jewersphysio" class="social-item  icon-twitter" title="External link to Louise Jewers Twitter profile.">
                <svg class="svg--twitter-dims">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#twitter"></use>
                </svg>
              </a>
            </div>
            <p>Louise Jewers Physiotherapy - Chiswick Physiotherapists | &copy;<script type="text/javascript">document.write(new Date().getFullYear());</script>.</p>
          </div>
          <!-- END : FOOTER ITEM -->
        </div>
        <!-- END : FLEX CONTAINER -->
        <!-- BEGIN : BOTTOM FOOTER -->
        <div class="bottom-footer">
          <p>Powered by <a href="http://www.physio123.co.uk/"><b>Physio123</b></a></p>
        </div>
        <!-- END : BOTTOM FOOTER -->
      </div>
      <!-- END : COL -->
    </div>
    <!-- END : ROW -->
  </div>
  <!-- END : CONTAINER -->
</footer>
<!--******************** SITE FOOTER : End ********************-->


<!--/ Scroll to top -->
<a href="#" class="scroll-to-top-btn"></a>


<!-- JS SCRIPT -->
<?php if($current == 'contact') {echo '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>';} ?>
<script src="js/main.js"></script>
<script src="twitter/get-tweets.js"></script>
<script type="text/javascript">
    //get JSON object from twitter
    var tweets = <?php echo GetTweets::get_most_recent('AlexMClapperton','1','true') ?>;
    //pass returned JSON object into display_tweets()
    display_tweets(tweets);
</script>
