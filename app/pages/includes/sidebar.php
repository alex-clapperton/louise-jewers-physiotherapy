<?php
  // Requiring custom class that pulls most recent tweets
  require_once('twitter/get-tweets.php');
 ?>

<!-- BEGIN : SITE SIDEBAR -->
<aside class="site-sidebar">
  <!-- BEGIN : BOX -->
  <div <?php if($hide == 'true') {echo 'class="hide"';} else {echo 'class="box  testimonial-box"';} ?>>
    <h4>Testimonial</h4>
    <blockquote>
      <span>&#8220;</span>

      <?php
        if($current == 'home') {
          echo "
            Louise knows her subject inside out and is very perceptive. She is quick to identify the precise nature of the pain and is normally able to have me up and running again after a couple of visits.
            <br><br>
            I would not hesitate to recommend her to anyone who requires a physio. I have not come across a better one in 25 years of sports related injuries.
          ";
        }
        if ($current == 'treat') {
          echo "
            Over the past few years Louise has provided excellent treatment and prescribed effective exercises to realign my wobbly knees and heal a bruised Achilles tendon. She’s now applying her skills to my tennis elbow. She has been a key factor in keeping this 66 year old body active.
          ";
        }
        if ($current == 'why') {
          echo "
            As you can see I have cancelled my next appointment. This is because my foot has completely recovered! Thank you for your two treatments- I think they helped it along quite considerably.
          ";
        }
        if ($current == 'conditions') {
          echo "
            Thank you for your letter of 3rd January and thank you in particular for treating the patients I send to you to such good effect. I hear very favourable reports from patients following tour treatments.
          ";
        }
        if ($current == 'appointments') {
          echo "
            Thanks to you my knees are as good as they have ever been. I cannot tell that I have ever had any trouble. Fantastic! My skiing still leaves a lot to be desired but great fun!
          ";
        }
        if ($current == 'contact') {
          echo "
            Many thanks for your help with my back. Your advice was 100% good I was sent off for xrays - crush fractures were the problem… Many thanks for all your help in setting me off in the right direction.
          ";
        }
      ?>

    </blockquote>
    <cite>

      <?php
        if($current == 'home') {
          echo "<b>Marcus Buckland</b>, <br> Sky Sports Presenter";
        }
        if($current == 'treat') {
          echo "<b>Bill Nemtin</b>, <br> Chiswick";
        }
        if($current == 'why') {
          echo "<b>C.F</b>";
        }
        if($current == 'conditions') {
          echo "<b>Consultant Orthopaedic Surgeon</b>";
        }
        if($current == 'appointments') {
          echo "<b>G.N</b>";
        }
        if($current == 'contact') {
          echo "<b>R.D</b>";
        }
      ?>

    </cite>
  </div>
  <!-- END : BOX -->
  <!-- BEGIN : BOX -->
  <div class="box  question-box">
    <h4>Got a question?</h4>
    <p>Or to book an appointment call <b>020 8742 7423</b>.</p>
  </div>
  <!-- END : BOX -->
  <!-- BEGIN : BOX -->
  <div class="box  find-box">
    <h4>How to find us</h4>

    <p>We are based at Hogarth Health Club, Airedale Ave, Chiswick, W4 2NW. Find us by:</p>

    <ul class="list  fa-ul">
      <li>
        <svg class="svg--checkmark-dims">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
        </svg>
        <a href="#">Car</a>
      </li>
      <li>
        <svg class="svg--checkmark-dims">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
        </svg>
        <a href="#">Bus</a>
      </li>
      <li>
        <svg class="svg--checkmark-dims">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
        </svg>
        <a href="#">Tube</a>
      </li>
    </ul>

    <p>or click to <a href="#">see a map</a></p>
  </div>
  <!-- END : BOX -->
  <!-- BEGIN : BOX -->
  <div class="box  area-box">
    <h4>Areas we cover</h4>

    <p>West London, Chiswick, Hammersmith, Acton, Shepherds Bush, Brentford, Barnes and Kew</p>
  </div>
  <!-- END : BOX -->
  <!-- BEGIN : BOX -->
  <div <?php if($current == 'home') {echo 'class="hide  box  appointment-box"';} else {echo 'class="box  appointment-box"';} ?>>
    <h4>first appointment?</h4>

    <p>Find out more information about what happens on your first visit... <br> <a href="#">Read more</a></p>
  </div>
  <!-- END : BOX -->
  <div class="box  twitter-feed">
    <h4><a href="https://twitter.com/jewersphysio" title="External link to Louise Jewers Twitter profile.">@jewersphysio</a></h4>
    <span class="tweet-loader">Loading tweets...</span>
  </div>
</aside>
<!-- END : SITE SIDEBAR -->
