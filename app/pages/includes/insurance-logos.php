<!--******************** INSURANCE LOGOS : Start ********************-->
<section class="insurance-logos">
  <div class="container">
    <div class="row">
      <div class="flex-container">
        <div class="insurance-img">
          <img src="images/insurance/physio-first.png" alt="Physio First logo." />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/csp.png" alt="" />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/hpcheck.png" alt="hpcheck.org logo." />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/macp.png" alt="" />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/aacp.png" alt="" />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/aviva.png" alt="Aviva logo." />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/simply-health.png" alt="Simply Health logo." />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/bupa.png" alt="Bupa Insurance logo" />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/wpa.png" alt="" />
        </div>
        <div class="insurance-img">
          <img src="images/insurance/cigna.png" alt="Cigna logo." />
        </div>
      </div>
      <p>Louise Jewers – Chartered Physiotherapist MCSP, Member of the Health Professions Council HPC, Physiofirst &amp; MACP, AACP</p>
    </div>
  </div>
</section>
<!--******************** INSURANCE LOGOS : End ********************-->
