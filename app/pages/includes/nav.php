<!--******************** SITE HEADER : Start ********************-->
<header class="site-header">
  <a href="#nav" class="skip-link  visually-hidden  focusable">Skip to navigation</a>
  <a href="#content" class="skip-link  visually-hidden  focusable  scroll-to" data-offset-top="130">Skip to content</a>
  <!-- BEGIN : TOP HEADER -->
  <div class="top-header">
    <!-- BEGIN : CONTAINER -->
    <div class="container">
      <!-- BEGIN : ROW -->
      <div class="row">
        <!-- BEGIN : FLEX CONTAINER -->
        <div class="flex-container">
          <ul>
            <li>
              <svg class="svg--call-dims">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#call"></use>
              </svg>
              <b>020 8742 7423</b></li>
            <li>
              <svg class="svg--mail-dims">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#mail"></use>
              </svg>
              <a href="mailto:info@louisejewersphysio.co.uk">info@louisejewersphysio.co.uk</a></li>
            <li>
              <svg class="svg--map-marker-dims">
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#map-marker"></use>
              </svg>
              <a href="#">Louise Jewers Physiotherapy, Hogarth Health Club</a></li>
          </ul>
          <ul>
            <li>
              <a href="https://twitter.com/jewersphysio" title="External link to Louise Jewers Twitter profile.">
                <svg class="svg--twitter-dims">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#twitter"></use>
                </svg>
              </a>
            </li>
          </ul>
        </div>
        <!-- END : FLEX CONTAINER -->
      </div>
      <!-- END : ROW -->
    </div>
    <!-- END : CONTAINER -->
  </div>
  <!-- END : TOP HEADER -->
  <!-- BEGIN : CONTAINER -->
  <div class="container">
    <!-- BEGIN : ROW -->
    <div class="row">
      <!-- BEGIN : MAIN HEADER -->
      <div class="main-header">
        <a href="index.php" class="logo"><img src="images/logo.svg" alt="Louise Jewers Physiotherapy logo" /> <small>Louise Jewers <br> <span>Physio</span></small></a>
        <!-- BEGIN : Mobile Nav -->
        <button type="button" name="button" class="trigger-nav">
          <div class="inner">
            <span class="icon-bar  top"></span>
            <span class="icon-bar  middle"></span>
            <span class="icon-bar  bottom"></span>
          </div>
        </button>
        <!-- END : Mobile Nav -->
        <!-- BEGIN : NAV -->
        <nav class="site-nav">
          <a href="index.php" <?php if($current == 'home') {echo 'class="active"';} ?>>Home</a>
          <a href="how-i-treat.php" <?php if($current == 'treat') {echo 'class="active"';} ?>>How I Treat</a>
          <a href="why-choose-me.php" <?php if($current == 'why') {echo 'class="active"';} ?>>Why Choose Me</a>
          <a href="conditions.php" <?php if($current == 'conditions') {echo 'class="active"';} ?>>Conditions</a>
          <a href="appointments.php" <?php if($current == 'appointments') {echo 'class="active"';} ?>>Appointments</a>
          <a href="testimonials.php" <?php if($current == 'testimonials') {echo 'class="active"';} ?>>Testimonials</a>
          <a href="contact.php" <?php if($current == 'contact') {echo 'class="active"';} ?>>Contact Us</a>
        </nav>
        <!-- END : NAV -->
      </div>
      <!-- END : MAIN HEADER -->
    </div>
    <!-- END : ROW -->
  </div>
  <!-- END : CONTAINER -->
</header>
<!--******************** SITE HEADER : End ********************-->
