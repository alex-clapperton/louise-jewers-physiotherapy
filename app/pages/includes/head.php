<head>

  <!-- META TAGS  -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php echo $meta_description; ?>">
  <meta name="keywords" content="<?php echo $meta_keywords; ?>">

  <!-- FAVICON -->
  <link rel="icon" type="image/ico" href="images/favicon.ico">

  <!-- PAGE TITLE -->
  <title><?php echo $page_title; ?></title>

  <!-- STYLESHEET -->
  <link rel="stylesheet" type="text/css" href="css/main.css">

  <!-- GOOGLE RECAPTCHA -->
  <?php if($current == 'contact') {echo "<script src='https://www.google.com/recaptcha/api.js'></script>";} ?>

</head>
