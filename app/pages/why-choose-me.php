<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/includes/";
   $current = 'why';
   $hide = 'false';
?>

<!DOCTYPE html>
<html lang="en">

  <!-- META TAGS  -->
  <?php $meta_description = '';?>
  <?php $meta_keywords = 'physiotherapy, Chiswick physiotherapy clinic, Chiswick physiotherapy, Chiswick physio, merseyside, north west, sports, sport, physical therapy, physiotherapist, physiotherapists in Chiswick, mobile physio, tennis elbow, pain, back pain, neck pain, back, spine, rehab, acupuncture, electrotherapy, ultrasound, advice, free, massage, joint, whiplash, medical insurance, bupa, ppp, csp, hpc, accredited, private physiotherapists, private, ocppp, physio first, clinics, stroke rehab, hospital, rehabilitation, golfers elbow, electro-therapy, headache, frozen shoulder, exercise, personal exercise program, state registered physiotherapist, health, health professional council, physio2u';?>

  <!-- PAGE TITLE -->
  <?php $page_title = "Louise Jewers Physiotherapy - Chiswick Physiotherapists";?>

  <!--========================================== - HEAD - ==========================================-->
  <?php include_once($path."head.php"); ?>

  <body>

    <!--========================================== - NAV - ==========================================-->
    <?php include_once($path."nav.php"); ?>


    <!--******************** SITE BANNER : Start ********************-->
    <section class="site-banner  small--banner  home">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <h1>Why Choose Me</h1>

              <p>My expertise and experience.</p>

              <div class="breadcrumbs">
                <a href="/bradfords/index.php">Home</a>
                <span>Why Choose Me</span>
              </div>

              <div class="scroll-down">
                <a href="#content" class="scroll-to" data-offset-top="130">
                  <svg class="svg--angle-down-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#angle-down"></use>
                  </svg>
                </a>
              </div>
              <!--/ Scroll DOWN -->
            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </section>
    <!--******************** SITE BANNER : End ********************-->


    <!--******************** SITE MAIN : Start ********************-->
    <main class="site-main">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <!-- BEGIN : SITE ARTICLE -->
              <article id="content" class="site-article">
                <span class="subtitle">Why Choose Me</span>
                <h2>Expertise &amp; Experience</h2>

                <p><b>I work in partnership with my patients</b> putting their needs at the core of my diagnosis, treatment and rehabilitation programme. My programmes are designed to develop, maintain and restore optimal movement and functional ability to you. Whatever your specific condition and circumstances, I can <b>work with you</b> to explain the diagnosis, create a suitable treatment plan and encourage and <b>motivate you</b> back to <b>good health</b>.</p>

                <p><b>My expertise and experience is extensive &amp; wide ranging:</b></p>

                <ul class="list">
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Qualified in 1980 and so now have 30 years experience
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Extensive experience in NHS and private medicine
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Specialist experience from The Royal National Orthopaedic in Stanmore working on the knee, shoulder, scoliosis and spinal injury units
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Responsibility for running NHS outpatients department
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Specialist knowledge in sports injuries gained from a 10 year association with Rosslyn Park Rugby Club which involved both on pitch first aid and diagnosis as well as ongoing treatment programmes for injuries
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Member of the Manipulative Association of Chartered Physiotherapists (MACP) and have been a clinical tutor and examiner for this organization
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Continual development of my knowledge and expertise by regular attendance at master classes, courses and peer review groups to ensure my clinical skills are evidence based and up to date
                  </li>
                </ul>

                <p><b>My clinic provides a bespoke, individual service</b> for both Hogarth club and non club members in an environment that offers access to all the facilities* required for successful relief from pain, recovery and rehabilitation.</p>

                <ol class="number-list">
                  <li><span>1.</span> No waiting list</li>
                  <li><span>2.</span> Saturday appointments</li>
                  <li><span>3.</span> Free onsite parking</li>
                  <li><span>4.</span> Dedicated treatment room</li>
                  <li><span>5.</span> Latest equipment</li>
                  <li><span>6.</span> 2 well equipped gym areas</li>
                  <li><span>7.</span> Swimming pool</li>
                </ol>

                <p>* non members when supervised by myself during their appointment.</p>

                <p><b>I believe that mutual understanding, trust and good communication</b> between myself and my patients is fundamental to a <b>successful outcome</b> and my patient centred approach ensures a true partnership. To this end <b>I use a blend of manual therapy, exercise and self hep strategies</b>.</p>

                <div class="info-box">
                  <p>If you <b>want more information, have any questions</b> or are just not sure whether you will benefit from physiotherapy, please do <b>call me, without obligation</b>, on <b>020 8742 7423</b> or email <a href="mailto:info@louisejewersphysio.co.uk">info@louisejewersphysio.co.uk</a>.</p>

                  <p>If I am not available I will call you back as soon as possible.</p>
                </div>
              </article>
              <!-- END : SITE ARTICLE -->

              <!--========================================== - SIDEBAR - ==========================================-->
              <?php include_once($path."sidebar.php"); ?>

            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </main>
    <!--******************** SITE MAIN : End ********************-->


    <!--========================================== - INSURANCE LOGOS - ==========================================-->
    <?php include_once($path."insurance-logos.php"); ?>


    <!--========================================== - FOOTER - ==========================================-->
    <?php include_once($path."footer.php"); ?>

  </body>
</html>
