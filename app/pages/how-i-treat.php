<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/includes/";
   $current = 'treat';
   $hide = 'false';
?>

<!DOCTYPE html>
<html lang="en">

  <!-- META TAGS  -->
  <?php $meta_description = '';?>
  <?php $meta_keywords = 'physiotherapy, Chiswick physiotherapy clinic, Chiswick physiotherapy, Chiswick physio, merseyside, north west, sports, sport, physical therapy, physiotherapist, physiotherapists in Chiswick, mobile physio, tennis elbow, pain, back pain, neck pain, back, spine, rehab, acupuncture, electrotherapy, ultrasound, advice, free, massage, joint, whiplash, medical insurance, bupa, ppp, csp, hpc, accredited, private physiotherapists, private, ocppp, physio first, clinics, stroke rehab, hospital, rehabilitation, golfers elbow, electro-therapy, headache, frozen shoulder, exercise, personal exercise program, state registered physiotherapist, health, health professional council, physio2u';?>

  <!-- PAGE TITLE -->
  <?php $page_title = "Louise Jewers Physiotherapy - Chiswick Physiotherapists";?>

  <!--========================================== - HEAD - ==========================================-->
  <?php include_once($path."head.php"); ?>

  <body>

    <!--========================================== - NAV - ==========================================-->
    <?php include_once($path."nav.php"); ?>


    <!--******************** SITE BANNER : Start ********************-->
    <section class="site-banner  small--banner  home">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <h1>How I Treat</h1>

              <p>The treatments and techniques I use.</p>

              <div class="breadcrumbs">
                <a href="/bradfords/index.php">Home</a>
                <span>How I Treat</span>
              </div>

              <div class="scroll-down">
                <a href="#content" class="scroll-to" data-offset-top="130">
                  <svg class="svg--angle-down-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#angle-down"></use>
                  </svg>
                </a>
              </div>
              <!--/ Scroll DOWN -->
            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </section>
    <!--******************** SITE BANNER : End ********************-->


    <!--******************** SITE MAIN : Start ********************-->
    <main class="site-main">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <!-- BEGIN : SITE ARTICLE -->
              <article id="content" class="site-article">
                <span class="subtitle">How I Treat</span>
                <h2>Treatments &amp; Techniques</h2>

                <div class="img-left">
                  <img src="images/louise-jewers.jpg" alt="Portrait shot of Louise Jewers on the right wearing a white shirt, bending a clients leg, with the client lying down, wearing a blue shirt. The clients head is not in the image." />
                </div>

                <h3>What is physiotherapy?</h3>

                <blockquote>
                  <span>&#8220;</span>
                  "Physiotherapy uses physical approaches to promote, maintain and restore physical, psychological and social well being, taking account of variations in health status. It is science based, committed to extending, applying, evaluating and reviewing the evidence that underpins and informs its practice and delivery. The exercise of clinical judgment and informed interpretation is at its core."
                </blockquote>
                <cite><a href="http://www.csp.org.uk" title="External link to the csp.org website.">www.csp.org.uk</a></cite>

                <h3>What do I treat?</h3>

                <p>I am an expert in human movement and recognise that functional movement is a central aspect of what it means to be healthy. Movement and function can be threatened by aging, injury, disease and environmental factors. <b>My professional training, high level of expertise and years of experience</b> mean I can address issues caused by any of these things and apply my knowledge of anatomy, biomechanics, physiology and pathology to <b>diagnose the problem, explain why</b> it’s happened and <b>formulate an appropriate treatment plan</b>. If, through discussion or after assessment , I believe that physiotherapy is not the answer to your problem, I will be able to <b>refer you</b> to an appropriate medical or other health professional alternative.</p>

                <p>I specialise in the treatment of many <a href="#">conditions</a>, including <b>sports injuries, back and neck care, and joint, muscle and ligament problems</b> caused by any of the aforementioned factors. <b>My 5 point treatment plans focus on</b>:</p>

                <ol class="number-list">
                  <li><span>1.</span> Helping you understand the problem</li>
                  <li><span>2.</span> Providing pain relief</li>
                  <li><span>3.</span> Promoting early recovery</li>
                  <li><span>4.</span> Developing corrective exercises to do both with me and on your own</li>
                  <li><span>5.</span> Motivating you and inspiring confidence that will get you back to optimal health</li>
                </ol>

                <p><b>My key clinical interests include</b> the treatment of sports injuries, knee and shoulder conditions including post surgery rehabilitation, spinal conditions, posture and back care education &amp; preventative advice.</p>

                <h3>What techniques do I use?</h3>

                <p>Once I have undertaken a <b>full examination and assessment and agreed treatment goals</b> with you I will devise a treatment plan. As I am working with stressed joints, ligaments, muscles soft tissue and nerves, I apply a range of <b>tailored treatment techniques and solutions customised to your specific needs</b>. I may use:</p>

                <ul class="list">
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Manipulative therapy
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Mobilization therapy
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Soft tissue techniques
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Core exercises
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Electrotherapy
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Postural advice
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Acupuncture
                  </li>
                </ul>

                <p>as appropriate to the injury. </p>

                <p>And it does not stop at that – I believe it is imperative to help you understand what you need to do to prevent further injury. <b>To this end I will prescribe home exercises &amp; advice and recommend changes to training programmes and even office ergonomics to ensure you do not suffer a relapse</b>.</p>

                <div class="info-box">
                  <p>If you <b>want more information, have any questions</b> or are just not sure whether you will benefit from physiotherapy, please do <b>call me, without obligation</b>, on <b>020 8742 7423</b> or email <a href="mailto:info@louisejewersphysio.co.uk">info@louisejewersphysio.co.uk</a>.</p>

                  <p>If I am not available I will call you back as soon as possible.</p>
                </div>
              </article>
              <!-- END : SITE ARTICLE -->

              <!--========================================== - SIDEBAR - ==========================================-->
              <?php include_once($path."sidebar.php"); ?>

            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </main>
    <!--******************** SITE MAIN : End ********************-->


    <!--========================================== - INSURANCE LOGOS - ==========================================-->
    <?php include_once($path."insurance-logos.php"); ?>


    <!--========================================== - FOOTER - ==========================================-->
    <?php include_once($path."footer.php"); ?>

  </body>
</html>
