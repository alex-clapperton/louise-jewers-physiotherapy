<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/includes/";
   $current = 'appointments';
   $hide = 'false';
?>

<!DOCTYPE html>
<html lang="en">

  <!-- META TAGS  -->
  <?php $meta_description = '';?>
  <?php $meta_keywords = 'physiotherapy, Chiswick physiotherapy clinic, Chiswick physiotherapy, Chiswick physio, merseyside, north west, sports, sport, physical therapy, physiotherapist, physiotherapists in Chiswick, mobile physio, tennis elbow, pain, back pain, neck pain, back, spine, rehab, acupuncture, electrotherapy, ultrasound, advice, free, massage, joint, whiplash, medical insurance, bupa, ppp, csp, hpc, accredited, private physiotherapists, private, ocppp, physio first, clinics, stroke rehab, hospital, rehabilitation, golfers elbow, electro-therapy, headache, frozen shoulder, exercise, personal exercise program, state registered physiotherapist, health, health professional council, physio2u';?>

  <!-- PAGE TITLE -->
  <?php $page_title = "Louise Jewers Physiotherapy - Chiswick Physiotherapists";?>

  <!--========================================== - HEAD - ==========================================-->
  <?php include_once($path."head.php"); ?>

  <body>

    <!--========================================== - NAV - ==========================================-->
    <?php include_once($path."nav.php"); ?>


    <!--******************** SITE BANNER : Start ********************-->
    <section class="site-banner  small--banner  home">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <h1>Appointments</h1>

              <p>Book your appointment today.</p>

              <div class="breadcrumbs">
                <a href="/bradfords/index.php">Home</a>
                <span>Appointments</span>
              </div>

              <div class="scroll-down">
                <a href="#content" class="scroll-to" data-offset-top="130">
                  <svg class="svg--angle-down-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#angle-down"></use>
                  </svg>
                </a>
              </div>
              <!--/ Scroll DOWN -->
            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </section>
    <!--******************** SITE BANNER : End ********************-->


    <!--******************** SITE MAIN : Start ********************-->
    <main class="site-main">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <!-- BEGIN : SITE ARTICLE -->
              <article id="content" class="site-article">
                <span class="subtitle">Appointments</span>
                <h2>Early treatment and advice reduces the risk of chronic problems developing</h2>

                <h3>Self referral</h3>

                <p>Many patients refer themselves and come to me via word of mouth from the <b>personal recommendation of others</b> or from health professionals. All you need to do is call and book an appointment unless you wish to use health Insurance (see below).</p>

                <h3>Via a doctor or other heath professional</h3>

                <p>Please bring your referral letter or their contact details and any xrays or scans you may have.</p>

                <h3>Using your health insurance</h3>

                <p>I am an approved practitioner and provider by <b>all of the major insurers</b>. If you wish to use your health insurance to cover the cost of treatment, you are likely to need a doctors referral so you must:</p>

                <ul class="list">
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Check the conditions of your health insurance and arrange authorisation prior to commencing treatment if you wish to claim against it
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Bupa patients please bring your membership and authorisation numbers to your first appointment.
                  </li>
                </ul>

                <h2>First appointment</h2>

                <p>It is important that I really <b>understand your problem</b>, how it affects your life, how it responds to different circumstances and what you hope to achieve from your treatment. So your first visit will also include a <b>full examination and assessment</b>. This will enable us to <b>agree your rehabilitation goals</b> from which I will devise <b>your treatment plan</b>.</p>

                <ul class="list">
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Please allow time to arrive or park and register at reception before your appointment
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Please bring any referral letters, scans or x-rays with you if available
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Dress in loose appropriate clothing. You may need to remove some outer clothing
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Bring shorts for lower limb conditions and running shoes or trainers with you
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Appointments are normally all 40 minutes. Extended appointments can be arranged if required
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    *Bupa appointments are 45 minutes initial. 30 minutes follow up
                  </li>
                </ul>

                <p>To book your first appointment and to find out about my fees, call us on <b>020 8742 7423</b>!</p>

                <h2>Payment</h2>

                <ul class="list">
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Payment is required at each appointment except For BUPA who will be invoiced directly **</li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Credit, debit cards, cheques and cash are accepted</li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Receipted invoices are issued for you to claim back from your insurers</li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    24 hours notice of cancellation or appointment change is requested. Missed or late cancellations that remain unfilled will be charged to you, as will any shortfalls in insurance payments</li>
                </ul>

                <p class="credit-cards">
                  <svg class="svg--visa-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#visa"></use>
                  </svg>
                  <svg class="svg--switch-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#switch"></use>
                  </svg>
                  <svg class="svg--mastercard-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#mastercard"></use>
                  </svg>
                  <svg class="svg--solo-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#solo"></use>
                  </svg>
                  <span>We accept all major credit / debit cards.</span>
                </p>


                <div class="info-box">
                  <p>If you <b>want more information, have any questions</b> or are just not sure whether you will benefit from physiotherapy, please do <b>call me, without obligation</b>, on <b>020 8742 7423</b> or email <a href="mailto:info@louisejewersphysio.co.uk">info@louisejewersphysio.co.uk</a>.</p>

                  <p>If I am not available I will call you back as soon as possible.</p>
                </div>
              </article>
              <!-- END : SITE ARTICLE -->

              <!--========================================== - SIDEBAR - ==========================================-->
              <?php include_once($path."sidebar.php"); ?>

            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </main>
    <!--******************** SITE MAIN : End ********************-->


    <!--========================================== - INSURANCE LOGOS - ==========================================-->
    <?php include_once($path."insurance-logos.php"); ?>


    <!--========================================== - FOOTER - ==========================================-->
    <?php include_once($path."footer.php"); ?>

  </body>
</html>
