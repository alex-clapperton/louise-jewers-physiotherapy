<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/includes/";
   $current = 'contact';
   $hide = 'false';
?>

<!DOCTYPE html>
<html lang="en">

  <!-- META TAGS  -->
  <?php $meta_description = '';?>
  <?php $meta_keywords = 'physiotherapy, Chiswick physiotherapy clinic, Chiswick physiotherapy, Chiswick physio, merseyside, north west, sports, sport, physical therapy, physiotherapist, physiotherapists in Chiswick, mobile physio, tennis elbow, pain, back pain, neck pain, back, spine, rehab, acupuncture, electrotherapy, ultrasound, advice, free, massage, joint, whiplash, medical insurance, bupa, ppp, csp, hpc, accredited, private physiotherapists, private, ocppp, physio first, clinics, stroke rehab, hospital, rehabilitation, golfers elbow, electro-therapy, headache, frozen shoulder, exercise, personal exercise program, state registered physiotherapist, health, health professional council, physio2u';?>

  <!-- PAGE TITLE -->
  <?php $page_title = "Contact | Louise Jewers Physiotherapy - Chiswick Physiotherapists";?>

  <!--========================================== - HEAD - ==========================================-->
  <?php include_once($path."head.php"); ?>

  <body>

    <!--========================================== - NAV - ==========================================-->
    <?php include_once($path."nav.php"); ?>


    <!--******************** SITE BANNER : Start ********************-->
    <section class="site-banner  small--banner  home">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <h1>Contact</h1>

              <p>Get in touch with me today.</p>

              <div class="breadcrumbs">
                <a href="/bradfords/index.php">Home</a>
                <span>Contact</span>
              </div>

              <div class="scroll-down">
                <a href="#content" class="scroll-to" data-offset-top="130">
                  <svg class="svg--angle-down-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#angle-down"></use>
                  </svg>
                </a>
              </div>
              <!--/ Scroll DOWN -->
            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </section>
    <!--******************** SITE BANNER : End ********************-->


    <!--******************** SITE MAIN : Start ********************-->
    <main class="site-main">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <!-- BEGIN : SITE ARTICLE -->
              <article id="content" class="site-article">
                <span class="subtitle">Contact</span>
                <h2>Contact Louise Jewers</h2>

                <p>If you need to find more information about the conditions I treat or for any other type of enquiry, fell free to contact me using the methods below.</p>

                <!-- BEGIN : CONTACT DETAILS -->
                <div class="contact-details">
                  <!-- BEGIN : FLEX CONTAINER -->
                  <div class="flex-container">
                    <!-- BEGIN : CONTACT BOX -->
                    <div class="contact-box">
                      <!-- BEGIN : CONTACT DETAIL -->
                      <div class="contact-detail">
                        <svg class="svg--call-dims">
                          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#call"></use>
                        </svg>
                        <h4>Phone</h4>
                        <p><b>020 8742 7423</b></p>
                      </div>
                      <!-- END : CONTACT DETAIL -->
                    </div>
                    <!-- END : CONTACT BOX -->
                    <!-- BEGIN : CONTACT BOX -->
                    <div class="contact-box">
                      <!-- BEGIN : CONTACT DETAIL -->
                      <div class="contact-detail">
                        <svg class="svg--home-dims">
                          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#home"></use>
                        </svg>
                        <h4>Address</h4>
                        <ul class="contact-address">
                          <li>Hogarth Health Club</li>
                          <li>Airedale Avenue</li>
                          <li>Chiswick</li>
                          <li>W4 2NW</li>
                        </ul>
                      </div>
                      <!-- END : CONTACT DETAIL -->
                    </div>
                    <!-- END : CONTACT BOX -->
                    <!-- BEGIN : CONTACT BOX -->
                    <div class="contact-box">
                      <!-- BEGIN : CONTACT DETAIL -->
                      <div class="contact-detail">
                        <svg class="svg--mail-dims">
                          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#mail"></use>
                        </svg>
                        <h4>Email</h4>
                        <p><a href="mailto:office@silverphysio.co.uk">office@silverphysio.co.uk</a></p>
                      </div>
                      <!-- END : CONTACT DETAIL -->
                    </div>
                    <!-- END : CONTACT BOX -->
                  </div>
                  <!-- END : FLEX CONTAINER -->
                </div>
                <!-- END : CONTACT DETAILS -->

                <div id="map"></div>

                <h4>Contact Form</h4>

                <!-- BEGIN : CONTACT FORM -->
                <form class="contact-form contact-mailer" action="forms/mailer.php" method="post">
                  <!-- BEGIN : FLEX CONTAINER -->
                  <div class="flex-container">
                    <!-- BEGIN : FORM CONTROL -->
                    <div class="form-control">
                      <svg class="svg--user-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#user"></use>
                      </svg>
                      <input type="text" name="name" placeholder="Name" required="">
                    </div>
                    <!-- END : FORM CONTROL -->
                    <!-- BEGIN : FORM CONTROL -->
                    <div class="form-control">
                      <svg class="svg--mail-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#mail"></use>
                      </svg>
                      <input type="email" name="email" placeholder="Email" required="">
                    </div>
                    <!-- END : FORM CONTROL -->
                  </div>
                  <!-- END : FLEX CONTAINER -->
                  <!-- BEGIN : FORM CONTROL -->
                  <div class="form-control">
                    <svg class="svg--comment-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#comment"></use>
                    </svg>
                    <textarea name="message" placeholder="Message" required=""></textarea>
                  </div>
                  <!-- END : FORM CONTROL -->

                  <div class="g-recaptcha" data-sitekey="6LeeqCQTAAAAALXHIAGQ3xbl86VX07mIcpaYjgsy" required=""></div>

                  <button type="submit" name="submit" class="submit">Submit</button>

                  <div class="form-message"></div>
                </form>
                <!-- END : CONTACT FORM -->

                <div class="info-box">
                  <p>If you <b>want more information, have any questions</b> or are just not sure whether you will benefit from physiotherapy, please do <b>call me, without obligation</b>, on <b>020 8742 7423</b> or email <a href="mailto:info@louisejewersphysio.co.uk">info@louisejewersphysio.co.uk</a>.</p>

                  <p>If I am not available I will call you back as soon as possible.</p>
                </div>
              </article>
              <!-- END : SITE ARTICLE -->

              <!--========================================== - SIDEBAR - ==========================================-->
              <?php include_once($path."sidebar.php"); ?>

            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </main>
    <!--******************** SITE MAIN : End ********************-->


    <!--========================================== - INSURANCE LOGOS - ==========================================-->
    <?php include_once($path."insurance-logos.php"); ?>


    <!--========================================== - FOOTER - ==========================================-->
    <?php include_once($path."footer.php"); ?>

  </body>
</html>
