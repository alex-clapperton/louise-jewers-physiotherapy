<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/includes/";
   $current = 'testimonials';
   $hide = 'true';
?>

<!DOCTYPE html>
<html lang="en">

  <!-- META TAGS  -->
  <?php $meta_description = '';?>
  <?php $meta_keywords = 'physiotherapy, Chiswick physiotherapy clinic, Chiswick physiotherapy, Chiswick physio, merseyside, north west, sports, sport, physical therapy, physiotherapist, physiotherapists in Chiswick, mobile physio, tennis elbow, pain, back pain, neck pain, back, spine, rehab, acupuncture, electrotherapy, ultrasound, advice, free, massage, joint, whiplash, medical insurance, bupa, ppp, csp, hpc, accredited, private physiotherapists, private, ocppp, physio first, clinics, stroke rehab, hospital, rehabilitation, golfers elbow, electro-therapy, headache, frozen shoulder, exercise, personal exercise program, state registered physiotherapist, health, health professional council, physio2u';?>

  <!-- PAGE TITLE -->
  <?php $page_title = "Louise Jewers Physiotherapy - Chiswick Physiotherapists";?>

  <!--========================================== - HEAD - ==========================================-->
  <?php include_once($path."head.php"); ?>

  <body>

    <!--========================================== - NAV - ==========================================-->
    <?php include_once($path."nav.php"); ?>


    <!--******************** SITE BANNER : Start ********************-->
    <section class="site-banner  small--banner  home">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <h1>Testimonials</h1>

              <p>What my clients have to say.</p>

              <div class="breadcrumbs">
                <a href="/bradfords/index.php">Home</a>
                <span>Testimonials</span>
              </div>

              <div class="scroll-down">
                <a href="#content" class="scroll-to" data-offset-top="130">
                  <svg class="svg--angle-down-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#angle-down"></use>
                  </svg>
                </a>
              </div>
              <!--/ Scroll DOWN -->
            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </section>
    <!--******************** SITE BANNER : End ********************-->


    <!--******************** SITE MAIN : Start ********************-->
    <main class="site-main">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <!-- BEGIN : SITE ARTICLE -->
              <article id="content" class="site-article">
                <span class="subtitle">Testimonials</span>
                <h2>What my clients have to say</h2>

                <div class="testimonials  flex-container">
                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      I have known Louise since 2002 when I was suffering with serious lower back pain. Since then she has treated me with great success on several occasions for which I am eternally grateful.
                      <br><br>
                      Louise knows her subject inside out and is very perceptive. She is quick to identify the precise nature of the pain and is normally able to have me up and running again after a couple of visits.
                      <br><br>
                      Recently she also played a big role in my rehab after I tore part of the meniscus in my left knee. Though very busy she also appreciates that, when in pain, it is important that a patient is seen as quickly as possible and always makes every effort to fit you in.
                      <br><br>
                      I would not hesitate to recommend her to anyone who requires a physio. I have not come across a better one in 25 years of sports related injuries.
                    </blockquote>
                    <cite><b>Marcus Buckland</b>, <br> Sky Sports Presenter</cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      Thank you so much for your help, by the way my elbow is just perfect now!
                    </blockquote>
                    <cite><b>M.P</b></cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      As you can see I have cancelled my next appointment. This is because my foot has completely recovered! Thank you for your two treatments- I think they helped it along quite considerably.
                    </blockquote>
                    <cite><b>C.F</b></cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      Thank you for your letter of 3rd January and thank you in particular for treating the patients I send to you to such good effect. I hear very favourable reports from patients following tour treatments.
                    </blockquote>
                    <cite><b>Consultant Orthopaedic Surgeon</b></cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      Thanks to you my knees are as good as they have ever been. I cannot tell that I have ever had any trouble. Fantastic! My skiing still leaves a lot to be desired but great fun!
                    </blockquote>
                    <cite><b>G.N</b></cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      Many thanks for your help with my back. Your advice was 100% good I was sent off for xrays - crush fractures were the problem… Many thanks for all your help in setting me off in the right direction.
                    </blockquote>
                    <cite><b>R.D</b></cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      Thanks to you my knee is a lot better, not 100% but progressing. This is my view from my sitting room in Sark. You can imagine how wonderful the sea looks during a storm.
                    </blockquote>
                    <cite><b>F.B Sark</b></cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      Just wanted to drop you a line and say the knees were absolutely fine while skiing and afterwards now I'm doing the leg strengthening exercises. Had a great time and many thanks for all the work in getting me back active.
                    </blockquote>
                    <cite><b>G.M Chiswick</b></cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      I am very appreciative of the treatment that was completed on my knee. I would attend the clinic again! Thanks.
                    </blockquote>
                    <cite><b>R</b></cite>
                  </div>

                  <div class="testimonial-item">
                    <blockquote>
                      <svg class="svg--quotes-left-dims">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#quotes-left"></use>
                      </svg>
                      I have just returned fro Arizona where I climbed a mountain and swam breastroke in the pool with no problems. So I have made a full recovery. THANK YOU ONCE AGAIN!
                    </blockquote>
                    <cite><b>D.C</b></cite>
                  </div>
                </div>

                <div class="info-box">
                  <p>If you <b>want more information, have any questions</b> or are just not sure whether you will benefit from physiotherapy, please do <b>call me, without obligation</b>, on <b>020 8742 7423</b> or email <a href="mailto:info@louisejewersphysio.co.uk">info@louisejewersphysio.co.uk</a>.</p>

                  <p>If I am not available I will call you back as soon as possible.</p>
                </div>
              </article>
              <!-- END : SITE ARTICLE -->

              <!--========================================== - SIDEBAR - ==========================================-->
              <?php include_once($path."sidebar.php"); ?>

            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </main>
    <!--******************** SITE MAIN : End ********************-->


    <!--========================================== - INSURANCE LOGOS - ==========================================-->
    <?php include_once($path."insurance-logos.php"); ?>


    <!--========================================== - FOOTER - ==========================================-->
    <?php include_once($path."footer.php"); ?>

  </body>
</html>
