<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/includes/";
   $current = 'home';
   $hide = 'false';
?>

<!DOCTYPE html>
<html lang="en">

  <!-- META TAGS  -->
  <?php $meta_description = 'Louise Jewers Physio - Over 10 years providing friendly, evidence based physiotherapy &amp; sports injury solutions  to the Chiswick community and beyond.';?>
  <?php $meta_keywords = 'physiotherapy, Chiswick physiotherapy clinic, Chiswick physiotherapy, Chiswick physio, merseyside, north west, sports, sport, physical therapy, physiotherapist, physiotherapists in Chiswick, mobile physio, tennis elbow, pain, back pain, neck pain, back, spine, rehab, acupuncture, electrotherapy, ultrasound, advice, free, massage, joint, whiplash, medical insurance, bupa, ppp, csp, hpc, accredited, private physiotherapists, private, ocppp, physio first, clinics, stroke rehab, hospital, rehabilitation, golfers elbow, electro-therapy, headache, frozen shoulder, exercise, personal exercise program, state registered physiotherapist, health, health professional council, physio2u';?>

  <!-- PAGE TITLE -->
  <?php $page_title = "Louise Jewers Physiotherapy - Chiswick Physiotherapists";?>

  <!--========================================== - HEAD - ==========================================-->
  <?php include_once($path."head.php"); ?>

  <body>

    <!--========================================== - NAV - ==========================================-->
    <?php include_once($path."nav.php"); ?>


    <!--******************** SITE BANNER : Start ********************-->
    <section class="site-banner  home">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <h1>Welcome To Louise Jewers Physiotherapy</h1>

              <p>Located within the Hogarth Health Club in Chiswick, West London.</p>

              <div class="btn-block">
                <a href="#">How I Treat</a>
                <a href="#">Why Choose Me</a>
              </div>
              <!--/ Btn block -->

              <div class="scroll-down">
                <a href="#content" class="scroll-to" data-offset-top="130">
                  <svg class="svg--angle-down-dims">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#angle-down"></use>
                  </svg>
                </a>
              </div>
              <!--/ Scroll DOWN -->
            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </section>
    <!--******************** SITE BANNER : End ********************-->


    <!--******************** SITE MAIN : Start ********************-->
    <main class="site-main">
      <!-- BEGIN : CONTAINER -->
      <div class="container">
        <!-- BEGIN : ROW -->
        <div class="row">
          <!-- BEGIN : COL -->
          <div class="col">
            <!-- BEGIN : FLEX CONTAINER -->
            <div class="flex-container">
              <!-- BEGIN : SITE ARTICLE -->
              <article id="content" class="site-article">
                <span class="subtitle">Chiswick Physiotherapists</span>
                <h2>About Louise Jewers Physiotherapy</h2>

                <div class="img-left">
                  <img src="images/treatment-room.jpg" alt="Wide shot of a treatment room at Louise Jewers Physio, with a treatment bed in the centre and a computer desk to the left." />
                </div>

                <p>As a Chartered Physiotherapist, with over <b>30 years experience in sports injury, muscoskeletal, spinal and orthopaedic treatment and rehabilitation</b>, I set the clinic up over 10 years ago, with a <b>mission to place the needs of my patients at the very heart of the service</b> I offer. My aim was to provide <b>evidence based physiotherapy and sports injury solutions in a professional, supportive and friendly environment</b>.</p>

                <p>I chose the Hogarth club as a location because it provides me with unique access to marvelous treatment facilities, including 2 fully equipped gym areas and a pool, neither of which are available from many physiotherapy services, yet I believe can be essential to effective recovery. This partnership of my experience and the Hogarth location, focused exclusively on my patients’ needs, has contributed to the wide ranging success I have had with hundreds of patients over the last decade.</p>

                <p>Whatever your problems and symptoms, I have the expertise and experience to clinically diagnose, treat and successfully rehabilitate a wide range of <a href="#">conditions</a> including, but not confined to:</p>

                <ul class="list">
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Sports injuries
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Back and neck pain
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Whiplash
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Joint, muscle and ligament problems
                  </li>
                  <li>
                    <svg class="svg--checkmark-dims">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="images/icons/sprite.svg#checkmark"></use>
                    </svg>
                    Post surgery physio requirements
                  </li>
                </ul>

                <p>I also specialise in providing advice and support on injury prevention.</p>

                <p>In addition to the facilities I have at my disposal, I believe in building <b>strong working relationships with other health professionals, medical specialists and local GPs</b> to ensure I have all your treatment needs covered. I am also <b>recognised by all the major health Insurance companies</b>.</p>

                <p>Whether young or old, very active or with restricted mobility, novice athlete or competitive sports person, everyone receives a warm welcome and professional treatment. <b>The large number of personal recommendations from satisfied patients and doctors is a testament to the quality of physiotherapy and rehabilitation you will receive</b>. Please do take the time to read on to get an idea of my approach and results.</p>

                <p>The location of my clinic makes it easy for anyone based in or around West London to get here and there is <b>ample free parking on site</b> as well as <b>disabled access</b>.</p>

                <div class="info-box">
                  <p>If you <b>want more information, have any questions</b> or are just not sure whether you will benefit from physiotherapy, please do <b>call me, without obligation</b>, on <b>020 8742 7423</b> or email <a href="mailto:info@louisejewersphysio.co.uk">info@louisejewersphysio.co.uk</a>.</p>

                  <p>If I am not available I will call you back as soon as possible.</p>
                </div>
              </article>
              <!-- END : SITE ARTICLE -->

              <!--========================================== - SIDEBAR - ==========================================-->
              <?php include_once($path."sidebar.php"); ?>

            </div>
            <!-- END : FLEX CONTAINER -->
          </div>
          <!-- END : COL -->
        </div>
        <!-- END : ROW -->
      </div>
      <!-- END : CONTAINER -->
    </main>
    <!--******************** SITE MAIN : End ********************-->


    <!--========================================== - INSURANCE LOGOS - ==========================================-->
    <?php include_once($path."insurance-logos.php"); ?>


    <!--========================================== - FOOTER - ==========================================-->
    <?php include_once($path."footer.php"); ?>

  </body>
</html>
