(function($) {

    'use strict';

    var AC = function() {
        this.VERSION = "1.1.0";
        this.AUTHOR = "Alex Clapperton";
        this.SUPPORT = "alexclapperton@nuttersons.co.uk";

        this.pageScrollElement = 'html, body';
        this.$body = $('body');

        this.setUserOS();
        this.setUserAgent();
    }

    // Set environment vars
    AC.prototype.setUserOS = function() {
        var OSName = "";
        if (navigator.appVersion.indexOf("Win") != -1) OSName = "windows";
        if (navigator.appVersion.indexOf("Mac") != -1) OSName = "mac";
        if (navigator.appVersion.indexOf("X11") != -1) OSName = "unix";
        if (navigator.appVersion.indexOf("Linux") != -1) OSName = "linux";

        this.$body.addClass(OSName);
    }

    AC.prototype.setUserAgent = function() {
        if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
            this.$body.addClass('mobile');
        } else {
            this.$body.addClass('desktop');
            if (navigator.userAgent.match(/MSIE 9.0/)) {
                this.$body.addClass('ie9');
            }
        }
    }

    // AC util functions
    AC.prototype.getUserAgent = function() {
        return $('body').hasClass('mobile') ? "mobile" : "desktop";
    }

    $.AC = new AC();
    $.AC.Constructor = AC;

})(window.jQuery);


(function($) {

    'use strict';

    // ======================================
    //    Dummy link
    // ======================================
    function init_dummy_link() {
      // Disable default link behavior for dummy links that have href='#'
      var $emptyLink = $('a[href=#]');
      $emptyLink.on('click', function(e){
        e.preventDefault();
      });
    }


    // ======================================
    //    Smooth Scroll to element
    // ======================================
  	function init_scroll_to() {
      var $scrollTo = $('.scroll-to');
    	$scrollTo.on('click', function(event){
    		var $elemOffsetTop = $(this).data('offset-top');
    		$('html').velocity("scroll", { offset:$(this.hash).offset().top-$elemOffsetTop, duration: 1500, easing:'easeInOutCubic', mobileHA: false});
    		event.preventDefault();
    	});
  	}


    // ======================================
    //    Smooth scroll to top button
    // ======================================
    function init_smooth_scroll_top() {
      // Animated Scroll to Top Button
      var $scrollTop = $('.scroll-to-top-btn');
      if ($scrollTop.length > 0) {
        $(window).on('scroll', function(){
          if ($(window).scrollTop() > 600) {
            $scrollTop.addClass('visible');
          } else {
            $scrollTop.removeClass('visible');
          }
        });
        $scrollTop.on('click', function(e){
          e.preventDefault();
          $('html').velocity("scroll", { offset: 0, duration: 1500, easing:'easeInOutCubic', mobileHA: false });
        });
      };
    }


    // ======================================
    //    Hide top header on scroll
    // ======================================
    function init_top_header_slide() {
      $(window).scroll(function() {
          if ($(this).scrollTop() >= 200) { // this refers to window
              $('.top-header').slideUp();
          }
          else if ($(this).scrollTop() <= 199) {
            $('.top-header').slideDown();
          }
      });
    }


    // ======================================
    //    Mobile navigation
    // ======================================
    function init_mobile_nav() {

      $('.trigger-nav').on('click', function() {

      	if (!$(this).hasClass('open-nav')) {
      		$(this).addClass('open-nav');
      		toggleNav(true);
      	} else {
      		$(this).removeClass('open-nav');
      		toggleNav(false);
      	}

      });

      function toggleNav(bool) {
      	if (bool === true) $('.site-nav').slideDown();
      	else $('.site-nav').slideUp();
      }
    }


    // ==============================================
    //    Snazzy map | Contact page
    // ==============================================
    function init_contact_map() {
      // When the window has finished loading create our google map below
      google.maps.event.addDomListener(window, 'load', init);

      function init() {
          // Basic options for a simple Google Map
          // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
          var mapOptions = {
              // How zoomed in you want the map to start at (always required)
              zoom: 15,

              scrollwheel: false,

              // The latitude and longitude to center the map (always required)
              center: new google.maps.LatLng(51.4919045,-0.2491013), // New York

              // How you would like to style the map.
              // This is where you would paste any style found on Snazzy Maps.
              styles: [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}]
          };

          // Get the HTML DOM element that will contain your map
          // We are using a div with id="map" seen below in the <body>
          var mapElement = document.getElementById('map');

          // Create the Google Map using our element and options defined above
          var map = new google.maps.Map(mapElement, mapOptions);

          // Following section, you can create your info window content using html markup
          var contentString =
              '<div id="content">'+
              '<h1>Louise Jewers Physiotherapy</h1>'+
              '<ul class="map-address">'+
              '<li>Hogarth Health Club</li>'+
              '<li>Airedale Avenue</li>'+
              '<li>Chiswick</li>'+
              '<li>W4 2NW</li>'+
              '</ul>'+
              '</div>';

          // Let's also add a marker while we're at it
          var marker = new google.maps.Marker({
              position: new google.maps.LatLng(51.4919045,-0.2491013),
              map: map,
              icon: 'http://localhost:8000/images/pointer.svg',
              title: "Louise Jewers Physiotherapy"
          });

          // Following Lines are needed if you use the Info Window to display content when map marker is clicked
          var infowindow = new google.maps.InfoWindow({
            content: contentString
          });

          // Following line turns the marker, into a clickable button and when clicked, opens the info window
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
          });
      }
    }


    // ==============================================
    //    Contact form | Contact page
    // ==============================================
    function init_contact_form() {
      var form = $('.contact-form'),
          // Get the messages div.
          formMessages = $('.form-messages');

      // Set up an event listener for the contact form.
      $(form).submit(function (e) {
        // Stop the browser from submitting the form.
        e.preventDefault();

        // Serialize the form data.
        var formData = $(form).serialize();

        // Submit the form using AJAX.
        $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: formData
        })
        .done(function (response) {
            // Make sure that the formMessages div has the 'success' class.
            $(formMessages).removeClass('error').addClass('success').fadeIn().delay(5000).fadeOut();
            // Set the message text.
            $(formMessages).text(response);

            // Clear the form.
            $(form).trigger("reset");
        })
        .fail(function (data) {
            // Make sure that the formMessages div has the 'error' class.
            $(formMessages).removeClass('success').addClass('error').fadeIn().delay(5000).fadeOut();
            // Set the message text.
            if (data.responseText !== '') {
                $(formMessages).text(data.responseText);
            } else {
                $(formMessages).text('Oops! An error occured and your message could not be sent.');
            }
        });
      });
    }


    // ==============================================
    //    Initialise plugins
    // ==============================================
    init_dummy_link();
    init_scroll_to();
    init_smooth_scroll_top();

    if (!$('init_mobile_nav').hasClass('desktop')) {
      init_mobile_nav();
    }
    if (document.URL.indexOf("contact.php") >= 0) {
      init_contact_map();
      init_contact_form();
    }
    if (!$('body').hasClass('mobile')) {
      init_top_header_slide();
    }

})(window.jQuery);
